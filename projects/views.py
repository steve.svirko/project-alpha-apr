from django.shortcuts import render
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
from django.http import HttpResponseRedirect


@login_required
def show_project_list(request):
    model_list = Project.objects.filter(members=request.user)
    context = {"project_list": model_list}
    return render(request, "list.html", context)


@login_required
def show_project_details(request, pk):
    model_instance = Project.objects.get(pk=pk)
    context = {"project_details": model_instance}
    return render(request, "project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.save()
            return HttpResponseRedirect(
                "/projects/{id}/".format(id=project.id)
            )
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "create_project.html", context)
