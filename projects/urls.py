from django.urls import path

from projects.views import (
    create_project,
    show_project_details,
    show_project_list,
)

urlpatterns = [
    path("", show_project_list, name="list_projects"),
    path("<int:pk>/", show_project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
