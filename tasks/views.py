
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from .forms import TaskForm, TaskisComplete
from .models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return HttpResponseRedirect("/projects/{id}/".format(id=task.id))
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "create_task.html", context)


@login_required
def show_task_list(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {"task_list": task_list}
    return render(request, "my_tasks.html", context)


@login_required
def update_task_complete(request, pk):
    model_instance = Task.objects.get(pk=pk)
    if request.method == "POST":
        form = TaskisComplete(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskisComplete(instance=model_instance)

    context = {"form": form}

    return render(request, "my_tasks.html", context)
